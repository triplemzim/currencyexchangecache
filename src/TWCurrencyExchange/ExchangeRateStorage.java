package TWCurrencyExchange;

import javafx.util.Pair;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class ExchangeRateStorage {
    private static Map<Pair<String, String>, ExchangeRate> exchangeRateCache;
    private static final int EXPIRY_TIME = 5000;

    // Singleton cache instance
    private static Map<Pair<String, String>, ExchangeRate> getExchangeRateCache() {
        if (Objects.isNull(exchangeRateCache)) {
            synchronized (ExchangeRateStorage.class) {
                if (Objects.isNull(exchangeRateCache)) {
                    exchangeRateCache = new ConcurrentHashMap<>();
                }
            }
        }
        return exchangeRateCache;
    }

    public static BigDecimal getExchangeRate(String fromCurrency, String toCurrency) {
        Pair<String, String> currencyPair = new Pair<>(fromCurrency, toCurrency);

        if (getExchangeRateCache().containsKey(currencyPair) &&
            !getExchangeRateCache().get(currencyPair).isExpired()) {
                return getExchangeRateCache().get(currencyPair).getExchangeRate();
        }

        synchronized (ExchangeRate.class) {
            if (getExchangeRateCache().containsKey(currencyPair)) {
                System.out.println("Expired Entry Found!");
            } else {
                System.out.println("Cache hit miss!");
            }
            ExchangeRate exchangeRate = new ExchangeRate(
                    String.valueOf(new Random().nextDouble() * 100), EXPIRY_TIME);
            getExchangeRateCache().put(currencyPair, exchangeRate);
            return exchangeRate.getExchangeRate();
        }
    }
}
