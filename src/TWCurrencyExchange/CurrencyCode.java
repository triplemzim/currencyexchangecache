package TWCurrencyExchange;

public enum CurrencyCode {
    USD, // United States Dollar
    EUR, // Euro
    JPY, // Japanese Yen
    GBP, // British Pound Sterling
    AUD, // Australian Dollar
    CAD, // Canadian Dollar
    CHF, // Swiss Franc
    CNY, // Chinese Yuan
    INR, // Indian Rupee
    BRL  // Brazilian Real
}
