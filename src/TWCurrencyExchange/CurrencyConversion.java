package TWCurrencyExchange;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CurrencyConversion {
    private final Lock lock = new ReentrantLock();

    public static void main(String[] args) {
        CurrencyConversion cc = new CurrencyConversion();
        cc.runLoadTest();
    }

    public void runLoadTest() {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        for (int i = 0; i < 5; i++) {
            executorService.submit(new ExchangeCurrencyRunnerTask());
        }

        executorService.shutdown();
    }

    class ExchangeCurrencyRunnerTask implements Runnable {
        @Override
        public void run() {
            List<CurrencyCode> cc = Arrays.asList(CurrencyCode.values());
            int i = 100;
            while (i-- > 0) {
                Collections.shuffle(cc);
                String from = cc.get(0).toString();
                String to = cc.get(1).toString();
                BigDecimal eRate = ExchangeRateStorage.getExchangeRate(from, to);
                synchronized (lock) {
                    System.out.println(from + "-" + to + ": " + eRate);
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
