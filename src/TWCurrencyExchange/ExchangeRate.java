package TWCurrencyExchange;


import java.math.BigDecimal;
import java.time.Instant;

public class ExchangeRate {
    private BigDecimal exchangeRate;
    private Instant timeCreated;
    private int expiryInMillis;

    public ExchangeRate(String exchangeRate) {
        this.exchangeRate = new BigDecimal(exchangeRate);
        timeCreated = Instant.now();
        // default expiry 10s
        expiryInMillis = 10000;
    }

    public ExchangeRate(String exchangeRate, int expiryInMillis) {
        this.exchangeRate = new BigDecimal(exchangeRate);
        timeCreated = Instant.now();
        this.expiryInMillis = expiryInMillis;
        timeCreated = Instant.now();
    }

    public BigDecimal getExchangeRate() {
        return this.exchangeRate;
    }

    public int getExpiryInMillis() {
        return expiryInMillis;
    }


    public boolean isExpired() {
        return Instant.now().toEpochMilli() - timeCreated.toEpochMilli() > expiryInMillis;
    }
}
